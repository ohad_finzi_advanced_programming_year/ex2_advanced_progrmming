#include "Ribosome.h"


/*
The function creates a Protein linked list from the given RNA transcript
Input: RNA string transcript to create the protein linked list
Output: a pointer to the created protein
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	std::string currNucleotide = "";
	AminoAcid currAminoAcid;

	Protein* newProtein = new Protein;
	newProtein->init();

	while (RNA_transcript.length() >= NUM_OF_NUCLEOTIDES)  //a loop that runs until the RNA transcript is bigger than the num of nucleotides
	{
		currNucleotide = RNA_transcript.substr(0, NUM_OF_NUCLEOTIDES);  //gets the first 3 letters from the RNA transcript which represents the nucleotides
		RNA_transcript = RNA_transcript.substr(NUM_OF_NUCLEOTIDES);  //deletes the first 3 letters from the RNA transcript which represents the nucleotides
		currAminoAcid = get_amino_acid(currNucleotide);  //gets the amino acid according to the nucleotide

		if (currAminoAcid != UNKNOWN)  //checks if the current amino acid isnt unknown
		{
			newProtein->add(currAminoAcid);  //if the amino acid is valid it will be added to the protein linked list
		}
		else
		{
			newProtein->clear();  //if the amino acid isnt valid it will delete all the protein linked list and set it to null
			break;
		}
	} 

	return newProtein;
}

