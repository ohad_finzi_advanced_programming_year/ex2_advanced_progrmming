#include "Nucleus.h"


/*
The functions inserts all the given variables inside the created Gene class
Input: start - the index of the first gene, end -  the index of the last gene, on_comppementary_dna_strand - checks if the gene is in the dna
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}


/*
This function is a getter function of the element start from the Gene class
Output: the Gene class start variable
*/
unsigned int Gene::get_start() const
{
	return this->_start;
}


/*
This function is a getter function of the element end from the Gene class
Output: the Gene class end variable
*/
unsigned int Gene::get_end() const
{
	return this->_end;
}


/*
This function is a getter function of the element on_complementary_dna_strand from the Gene class
Output: the Gene class on_complementary_dna_strand variable
*/
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}


/*
This function is a setter function of the element start from the Gene class
Input: a new variable insted of the start variable inside the Gene class
*/
void Gene::set_start(unsigned int start)
{
	this->_start = start;
}


/*
This function is a setter function of the element end from the Gene class
Input: a new variable insted of the end variable inside the Gene class
*/
void Gene::set_end(unsigned int end)
{
	this->_end = end;
}


/*
This function is a setter function of the element on_complementary_dna_strand from the Gene class
Input: a new variable insted of the on_complementary_dna_strand variable inside the Gene class
*/
void Gene::set_on_complementary_dna_strand(bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}


/*
The function inserts the given dna strand inside the created Nucleus and creats a completery dna strand according to the given dna strand
Input: the dna strand to insert int the created Nuclues
*/
void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = "";

	for (int i = 0; i < dna_sequence.length(); i++)  //a loop that runs on the regular dna strand string
	{
		if (dna_sequence[i] == 'G')  //changes the letter from 'G' to 'C'
		{
			this->_complementary_DNA_strand += 'C';
		}
		else if (dna_sequence[i] == 'C')  //changes the letter from 'C' to 'G'
		{
			this->_complementary_DNA_strand += 'G';
		}
		else if (dna_sequence[i] == 'T')  //changes the letter from 'T' to 'A'
		{
			this->_complementary_DNA_strand += 'A';
		}
		else if (dna_sequence[i] == 'A')  //changes the letter from 'A' to 'T'
		{
			this->_complementary_DNA_strand += 'T';
		}
		else
		{
			std::cerr << "All DNA strands must only have G,C,T,A letters" << std::endl;
			_exit(1);
		}
	}
}


/*
The funtion gets the RNA transcript of the current Nucleus class according to the given Gene class
Input: a Gene class that being used to creat the RAN transcript
Output: the RNA transcript of the current Nucleus
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA_transcript = "";
	std::string DNA_strand = this->_DNA_strand;

	if (gene.is_on_complementary_dna_strand())  //checks in what strand the RNA is in according to the gicen Gene class
	{
		DNA_strand = this->_complementary_DNA_strand;
	}

	//checks if the given start and end indexes of the Gene class are valid according to the DNA strand
	if (!(gene.get_start() < 0 || gene.get_start() >= DNA_strand.length() || gene.get_end() < gene.get_end() || gene.get_end() >= DNA_strand.length()))
	{
		for (int i = gene.get_start(); i <= gene.get_end(); i++)  //runs on part of the DNA strand according to the start and end indexes given from the Gene class, and adds each letter to the RNA transcript
		{
			if (DNA_strand[i] == 'T')  //changes the letter from 'T' to 'U'
			{
				RNA_transcript += 'U';
			}
			else
			{
				RNA_transcript += DNA_strand[i];  
			}
		}
	}

	return RNA_transcript;
}


/*
The function gets the reversed DNA strand from the current Nucleus class
Output: the reversed DNA strand 
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversed_DNA_strand = "";

	for (int i = this->_DNA_strand.length() - 1; i >= 0; i--)  //runs on the DNA strand from the last index 
	{
		reversed_DNA_strand += this->_DNA_strand[i];  //inserts each letter to the reversed DNA strand
	}

	return reversed_DNA_strand;
}


/*
The function gets the number of times the given condon appeares in the current Nucleus DNA strand
Input: the codon string
Output: the number of the times the cododn appeares in the current Nuclues DNA strand
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int num_of_codon_appearances = 0;
	unsigned int found = this->_DNA_strand.find(codon);  //founds the first time the coddon appears in the DNA strand

	while (found != std::string::npos)  //the loop runs until there are no more codons found in the DNA strand
	{
		found = this->_DNA_strand.find(codon, found + 1);  //finds the next codon in the DNA strand
		num_of_codon_appearances++;  //counts the number of times the codon appeared
	}

	return num_of_codon_appearances;
}

