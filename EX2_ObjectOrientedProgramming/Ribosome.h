#ifndef RIBOSOME_H
#define RIBOSOME_H

#include <iostream>
#include <string>
#include "Protein.h"


#define NUM_OF_NUCLEOTIDES 3 


class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;
};


#endif // RIBOSOME_H

