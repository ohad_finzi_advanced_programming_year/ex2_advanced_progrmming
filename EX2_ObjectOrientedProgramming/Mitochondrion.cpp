﻿#include "Mitochondrion.h"


/*
The function creates a new Mitochondrion class
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}


/*
The function checks if the given Protein linked list is able to create glucose according to the recipe
(ALANINE -> LEUCINE -> GLYCINE -> HISTIDINE -> LEUCINE -> PHENYLALANINE -> AMINO_CHAIN_END)
Input: a protein linked list which contains a bunch of amino acid nodes
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* currNode = protein.get_first();  //gets the first node of the amino acid linked list

	this->_has_glocuse_receptor = false;  //resets the variable
	if (currNode && currNode->get_data() == ALANINE)  //checks if the first node fits to the decription and not empty
	{
		currNode = currNode->get_next();  //gets the next node in the linked list
		if (currNode && currNode->get_data() == LEUCINE)  //check if the second node fits to the decription...
		{
			currNode = currNode->get_next();  //gets the next node in the linked list...
			if (currNode && currNode->get_data() == GLYCINE)
			{
				currNode = currNode->get_next();
				if (currNode && currNode->get_data() == HISTIDINE)
				{
					currNode = currNode->get_next();
					if (currNode && currNode->get_data() == LEUCINE)
					{
						currNode = currNode->get_next();
						if (currNode && currNode->get_data() == PHENYLALANINE)
						{
							currNode = currNode->get_next();
							if (currNode && currNode->get_data() == AMINO_CHAIN_END)
							{
								this->_has_glocuse_receptor = true;  //after all the nodes fits to the description, the variable changes to true
							}
						}
					}
				}
			}
		}
	}
}


/*
The function is a setter function in the Mitochondrion class for the variable glocuse_level
Input: the new glocuse level
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}


/*
The function checks if the current Mitochondrion class can produce ATP
Output: true if the current Mitochondrion class can produce ATP and false if not
*/
bool Mitochondrion::produceATP() const
{
	bool produceATP = false;
	
	//to produce ATP the glocuse level of the Mitochondrion class needs to be at least 50 and the glocuse receptor needs to be true
	if (this->_glocuse_level >= MIN_GLOCUSE_LEVEL && this->_has_glocuse_receptor)  
	{
		produceATP = true;
	}

	return produceATP;
}

