#ifndef MITOCHONDRION_H
#define MITOCHONDRION_H

#include <iostream>
#include <string>
#include "AminoAcid.h"
#include "Protein.h"

#define MIN_GLOCUSE_LEVEL 50


class Mitochondrion
{
private:
	//fields
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;

public:
	//methods
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;
};


#endif // MITOCHONDRION_H

