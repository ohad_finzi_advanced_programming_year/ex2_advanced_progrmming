#include "Cell.h"


void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocuse_receptor_gene.init(glucose_receptor_gene.get_start(), glucose_receptor_gene.get_end(), glucose_receptor_gene.is_on_complementary_dna_strand());
	this->_atp_units = 0;
	this->_mitochondrion.init();
}


bool Cell::get_ATP()
{
	bool ATP = false;

	std::string RNA_transcript = this->_nucleus.get_RNA_transcript(this->_glocuse_receptor_gene);  //step #1
	  
	Protein* protein = this->_ribosome.create_protein(RNA_transcript);  //step #2
	if (!protein)
	{
		std::cerr << "Couldn't find an amino acid nodes according to the given RNA transcript" << std::endl;
		_exit(1);
	}

	this->_mitochondrion.insert_glucose_receptor(*protein);  //step #3

	this->_mitochondrion.set_glucose(MIN_GLOCUSE_LEVEL);  //step #4

	if (ATP = this->_mitochondrion.produceATP())  //step #5
	{
		this->_atp_units = 100;  //step #6
	}

	//clears the allocated dynamic memory
	protein->clear();
	delete protein;

	return ATP;
}

