#ifndef CELL_H
#define CELL_H

#include <iostream>
#include <string>
#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"


class Cell
{
private:
	//fields
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocuse_receptor_gene;
	unsigned int _atp_units;

public:
	//methods
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();
};


#endif // CELL_H

