#include <iostream>
#include "Nucleus.h"
#include "AminoAcid.h"
#include "Protein.h"
#include "Ribosome.h"
#include "Mitochondrion.h"
#include "Cell.h"

// MEMORY LEAK CHECK
#define _CRTDBG_MAP_ALLOC 
#include <crtdbg.h>


void checkGene();
void checkNucleus();
void checkRibosome();
void checkMitochondrion();


int main()
{
	//checkGene();

	//checkNucleus();

	//checkRibosome();

	//checkMitochondrion();

	printf("Leaks: %d", _CrtDumpMemoryLeaks());  // To check memory leak

	return 0;
}


void checkGene()
{
	//creates a new Gene class
	Gene newGene;
	newGene.init(0, 10, true);

	//prints all Gene class elements
	std::cout << newGene.get_start() << "\n";
	std::cout << newGene.get_end() << "\n";
	std::cout << newGene.is_on_complementary_dna_strand() << "\n";

	//changes all Gene class elements
	newGene.set_start(1);
	newGene.set_end(9);
	newGene.set_on_complementary_dna_strand(false);

	//prints all Gene class elements
	std::cout << newGene.get_start() << "\n";
	std::cout << newGene.get_end() << "\n";
	std::cout << newGene.is_on_complementary_dna_strand() << "\n";
}


void checkNucleus()
{
	//creates a new Gene class
	Gene newGene;
	newGene.init(2, 6, true);

	//creates a new Nucleus class
	Nucleus newNucleus;
	newNucleus.init("AGAACATAACT");


	//tests all the Nucleus class funtions
	std::cout << newNucleus.get_RNA_transcript(newGene) << std::endl;
	std::cout << newNucleus.get_reversed_DNA_strand() << std::endl;
	std::cout << newNucleus.get_num_of_codon_appearances("AAC") << std::endl;
}


void checkRibosome()
{
	Ribosome newRibosome;
	std::string RNA_transcript = "UUUAUGUUC";

	Protein* newProtein = newRibosome.create_protein(RNA_transcript);  //tests the function create_protein from Ribosome class
	AminoAcidNode* curr = newProtein->get_first();

	//checks the function output using print
	while (curr)
	{
		std::cout << curr->get_data() << std::endl;
		curr = curr->get_next();
	}

	//clears the allocated dynamic memory
	newProtein->clear();
	delete newProtein;
}


void checkMitochondrion()
{
	//creates the protein receipe according to the RNA transcript
	Ribosome newRibosome;
	std::string RNA_transcript = "GCUUUAGGUCAUUUAUUUUAA";  //a glocuse recipe: GCUUUAGGUCAUUUAUUUUAA
	Protein* newProtein = newRibosome.create_protein(RNA_transcript);

	//creates new mitochondrion class
	Mitochondrion newMito;
	newMito.init();
	
	std::cout << newMito.produceATP() << std::endl;  //checks first possible outcome of the function

	newMito.set_glucose(50);
	newMito.insert_glucose_receptor(*newProtein);

	std::cout << newMito.produceATP() << std::endl;  //checks second possible outcome of the function

	//clears the allocated dynamic memory
	newProtein->clear();
	delete newProtein;
}